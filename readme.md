# Soal-Shift-Sisop-Modul-2-A13-2022

## daftar isi ##
- [Daftar Anggota Kelompok](#daftar-anggota-kelompok)
- [Nomer 1](#nomer-1)
    - [Soal  1.A](#soal-1a)
    - [Soal  1.B](#soal-1b)
    - [Soal  1.C](#soal-1c)
    - [Soal  1.D](#soal-1d)
- [Nomer 2](#nomer-2)
    - [Soal  2.A](#soal-2a)
    - [Soal  2.B](#soal-2b)
    - [Soal  2.C](#soal-2c)
    - [Soal  2.D](#soal-2d)
    - [Soal  2.E](#soal-2e)
- [Nomer 2](#nomer-3)
    - [Soal  3.A](#soal-3a)
    - [Soal  3.B](#soal-3b)
    - [Soal  3.C](#soal-3c)
    - [Soal  3.D](#soal-3d)

## Daftar Anggota Kelompok ##

NRP | NAMA | KELAS
--- | --- | ---
5025201004  | Danial Farros Maulana | SISOP C
5025201038    | Cholid Junoto | SISOP A
5025201220    | Davian Benito | SISOP A

## Nomer 1 ##
Mas Refadi adalah seorang wibu gemink.  Dan jelas game favoritnya adalah bengshin impek. Terlebih pada game tersebut ada sistem gacha item yang membuat orang-orang selalu ketagihan untuk terus melakukan nya. Tidak terkecuali dengan mas Refadi sendiri. Karena rasa penasaran bagaimana sistem gacha bekerja, maka dia ingin membuat sebuah program untuk men-simulasi sistem history gacha item pada game tersebut. Tetapi karena dia lebih suka nge-wibu dibanding ngoding, maka dia meminta bantuanmu untuk membuatkan program nya. Sebagai seorang programmer handal, bantulah mas Refadi untuk memenuhi keinginan nya itu. 

### Soal 1.a ###
Saat program pertama kali berjalan. Program akan mendownload file characters dan file weapons dari link yang ada dibawah, lalu program akan mengekstrak kedua file tersebut. File tersebut akan digunakan sebagai database untuk melakukan gacha item characters dan weapons. Kemudian akan dibuat sebuah folder dengan nama “gacha_gacha” sebagai working directory. Seluruh hasil gacha akan berada di dalam folder tersebut. 

Link database dapat di akses di link berikut [Database item character](https://drive.google.com/file/d/1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp/view) dan [Database item weapons](https://drive.google.com/file/d/1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT/view)

```c
void directorygenerator(){
	pid_t child_id;
  	child_id = fork();
  	if (child_id < 0) {
    	exit(EXIT_FAILURE);
  	}
	if (child_id != 0) {
    	folderdownload();
  	}
  	else {
		char *argv[][6] = {{"mkdir","gacha_gacha",NULL},
			{"mkdir","characters",NULL},
			{"mkdir","weapon",NULL}
		};

		for(int anjg=0; anjg<3;anjg++){
		    if(fork()==0) continue;
		    sleep(10);
		    execv("/bin/mkdir", argv[anjg]); 
		}
  	}
}
```
sebelum melakukan download file dilakukan persiapan folder dengan membuat folder gacha_gacha yang digunakan untuk menyimpan hasil gacha dan folder characters dan weapon untuk menyimpan file directory gacha.

pada fungsi di atas juga dilakukan pemanggilan fungsi `folderdownload();` yang digunakan untuk mendownload folder dari link yang sudah di sediakan

```c
void folderdownload(){
	pid_t child_id;
 	child_id = fork();
  	if (child_id < 0) {
		exit(EXIT_FAILURE);
	}

	if (child_id == 0) {
		unzipdownloader();
	} 
  	else {
    	char *argv1[][10] = {
			{ "wget","-q","--no-check-certificate","https://drive.google.com/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT&export=download", "-O","weapon.zip",NULL},
			{ "wget","-q","--no-check-certificate","https://drive.google.com/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp&export=download", "-O","characters.zip",NULL},
		};
		for(int anjg=0;anjg<2;anjg++){
			if(fork()==0) continue;
			execv("/bin/wget", argv1[anjg]);
		}
		sleep(15);
  	}
}
```
proses download dapat dilihat pada fungsi diatas, proses dilakukan dengan menggunakan fungsu execv(wget). selanjutnya akan di dapatkan file hasil downloadan dengan format zip, dan perlu dilakukan ekstraksi, sehingga di sini melakukan pemanggilan fungsi `unzipdownloader()`

```c
void unzipdownloader(){
	pid_t child_id;
	child_id = fork();
	
	if (child_id < 0) {
		exit(EXIT_FAILURE);
	}

	if (child_id == 0) {
	} 
	else {
		char *argv3[][10]= {
			{"unzip","-j","characters.zip","-d","./characters",NULL},
			{"unzip","-j","weapon.zip","-d","./weapon",NULL}
		};
	for(int anjg=0; anjg<3;anjg++){
			if(fork()==0) continue;
			sleep(10);
			execv("/bin/unzip", argv3[anjg]); 
		}
	}
}
```
dalam proses unziping dilakukan dengan menggunakan fungsi `execv(unzip)`, sehingga akan didapatkan 3 folder yakni gacha_gacha,  weapons dan characters dimana folder weapons dan characters berisi file json dan folder gacha_gacha tidak berisi

### Soal 1.b ###
Mas Refadi ingin agar setiap kali gacha, item characters dan item weapon akan selalu bergantian diambil datanya dari database. Maka untuk setiap kali jumlah-gacha nya bernilai genap akan dilakukan gacha item weapons, jika bernilai ganjil maka item characters. Lalu untuk setiap kali jumlah-gacha nya mod 10, maka akan dibuat sebuah file baru (.txt) dan output hasil gacha selanjutnya akan berada di dalam file baru tersebut. Dan setiap kali jumlah-gacha nya mod 90, maka akan dibuat sebuah folder baru dan file (.txt) selanjutnya akan berada didalam folder baru tersebut.  Sehingga untuk setiap folder, akan terdapat 9 file (.txt) yang didalamnya berisi 10 hasil gacha. Dan karena ini simulasi gacha, maka hasil gacha di dalam file .txt adalah ACAK/RANDOM dan setiap file (.txt) isi nya akan berbeda

### Soal 1.c ###
Format penamaan setiap file (.txt) nya adalah {Hh:Mm:Ss}_gacha_{jumlah-gacha}, misal 04:44:12_gacha_120.txt, dan format penamaan untuk setiap folder nya adalah total_gacha_{jumlah-gacha}, misal total_gacha_270. Dan untuk setiap file (.txt) akan memiliki perbedaan penamaan waktu output sebesar 1 second.

> penjelasan soal 1.b dan 1.c di gabung
```c
void waepongenerate(){
	DIR *d;
	struct dirent *dir;
	d = opendir("./weapon/");
	if (d){
	int count =0;
	while ((dir = readdir(d)) != NULL){
		if (strlen(dir->d_name) > 5){
			strcpy(weapon[count], dir->d_name);
			printf(">> %d.wep %s\n", count, weapon[count]);
			count++;
			}
		}
		//printf("%d",count);
	closedir(d);
	}
}

void chargenerate(){
	DIR *d;
	struct dirent *dir;
	d = opendir("./characters/");
	if (d){
	int count =0;
	while ((dir = readdir(d)) != NULL){
		if (strlen(dir->d_name) > 5){
			strcpy(characters[count], dir->d_name);
			printf(">> %d.char %s\n", count,characters[count]);
			count++;
			}
		}
	closedir(d);
	}
}
```
fungsi di atas berguna untuk melakukan menstore nama file yang berada pada folder characters dan weapons kedalam array, hal ini berfungsi untuk mempermudah dalam melakukan generate random gacha

**Fungsi Utama (Main Function)**
```c
		waepongenerate();
		int gachaxx = 0;
		int money = 79000;
		int foldercheck = 1;
		int temp;
	  	while (1){
	  		if (money <0){ 
			  printf("uang kenakalan sudah habis!!");
			  return 0;
			}
			printf("tekan banyak gacha unutuk maksiat, 0 jika sedang miskin\n");
		  	scanf("%d", &temp);
			if (temp == 0) 
				return 0;

		  	printf("selamat anda mendapatkan\n");
			for (int i = 0; i < temp ; i++){
				money = money - 160;
		  		if(temp%2==1)
					gachawangychr(money, gachaxx);
				else	
					gachawangy(money, gachaxx);
				gachaxx++;
				if (gachaxx %10 == 0){				
					store(gachaxx, foldercheck-1);
					sleep(1);
					if (foldercheck % 9 == 0){
						foldercheck = storefolder(foldercheck*10);
					}
					printf("++>>%d",foldercheck ) ;
					foldercheck++;
				}
			}
		}
```

fugsi utama memiliki alur kerja sebagai berikut 
1. menerima inputan n , dimana n = 0 untuk berhenti dari program dan n > 0 merupakan banyaknya jumlah gacha.
2. jika inputan n bilangan ganjil akan melakukan gacha character dengan memanggil fungsi `gachawangychr();`dan jika bilangan genap akan melakuka gacha weapon dengan memanggil fungsi `gachawangy()`.

    **Fungsi Gachawangy()**
    ```c
    void gachawangychr(int money, int gachaxx){
        char temp[10];
        char name[50];
        char hero[50];
        int rndgnet = (time(0)+rand())%47;
        srand(gachaxx);
        strcpy(hero, "./characters/");
        strcat(hero, characters[rndgnet]);
        sprintf(tempresult[gachaxx%10], "%d", gachaxx+1);
        strcat(tempresult[gachaxx%10], "_characters_");
        strcat(tempresult[gachaxx%10], raritychr(hero));
        strcat(tempresult[gachaxx%10], "_");
        strcat(tempresult[gachaxx%10], getnamechr(hero));
        strcat(tempresult[gachaxx%10], "_");
        sprintf(temp, "%d", money);
        strcat(tempresult[gachaxx%10], temp);
        printf(">>> %s\n", tempresult[gachaxx%10]);
    }
    ```
    proses random generate ditujukan untuk menggenerate random angka yang selanjutnya dapat digunakan untuk memanggil array dari character

    **Fungsi Gachawangy()**
    ```c
    void gachawangy(int money, int gachaxx){
        char temp[10];
        char hero[70];
        int rndgnet = (time(0)+rand())%128;
        srand(gachaxx);		
        strcpy(hero, "./weapon/");
        strcat(hero, weapon[rndgnet]);
        sprintf(tempresult[gachaxx%10], "%d", gachaxx+1);
        strcat(tempresult[gachaxx%10], "_Waepon_");
        strcat(tempresult[gachaxx%10], rarity(hero));
        strcat(tempresult[gachaxx%10], "_");
        strcat(tempresult[gachaxx%10], getname(hero));
        strcat(tempresult[gachaxx%10], "_");
        sprintf(temp, "%d", money);
        strcat(tempresult[gachaxx%10], temp);
        printf(">>> %s\n", tempresult[gachaxx%10]);
    }
    ```
	proses random generate ditujukan untuk menggenerate random angka yang selanjutnya dapat digunakan untuk memanggil array dari character

3. proses store hasil gacha setiap mod 10 ke dalam file (.txt) dilakukan dengan memanggil fungsi `store()`

	**Fungsi Store()**
	```c
	void store(int gacha, int foldercount){
		time_t t;
		t = time(NULL);
		struct tm tm;
		tm = *localtime(&t);
		char titletime[20];
		char tmp[15];
		sprintf(titletime, "%02d", (tm.tm_hour+14)%24);
		strcat(titletime, ":");
		sprintf(tmp, "%02d", tm.tm_min);
		strcat(titletime, tmp);
		strcat(titletime, ":");
		sprintf(tmp, "%02d", tm.tm_sec);
		strcat(titletime, tmp);
		strcat(titletime, "_gacha_");
		sprintf(tmp, "%d", gacha);
		strcat(titletime, tmp);
		strcpy(txtname[foldercount],"./gacha_gacha/");
		strcat(txtname[foldercount],titletime);
		
		FILE * fPtr;
		fPtr = fopen(titletime, "w");
		if(fPtr == NULL){
			printf("Unable to create file.\n");
		}
		else{
			for(int i = 0 ; i <10 ; i++){
				fputs(tempresult[i],fPtr);
				fputs("\n",fPtr);
			}
			
			fclose(fPtr);
			movefile(titletime,"./gacha_gacha");
			deletefile(titletime);	
		}
		for(int i = 0 ; i <10 ; i++)
			strcpy(tempresult[i], "");
	}
	```
	proses penamaan dilakukan dengan membuat nama pada array dan nantinya dihunakan sebagai inputan pada open file. untuk memudahkan dalam 		pemindahan folder, penulis melakukan penyimpanan nama file yang tergenerate pada array universal bernama `txtname[]`
	
	pada fungsi diatas terdapat fungsi cabang untuk memindahkan file dan menghapus file mengingat file yang terbuat akan setara tingkatannya dengan 	folder gacha_gacha, kedua fungsi tersebut akan di jelaskan di akhir soal.
4. untuk setiap 9 file akan dimausukkan pemindahan kedalam folder, pada bagian ini dilakukan dengan memanggil fungsi storefolder`storefolder()`;

	**Fungsi StoreFolder()**
	```c
	int storefolder(int gacha){
		char name[40];
		char path[70];
		char tmp[10];
		sprintf(tmp, "%d", gacha);
		strcpy(name,"./gacha_gacha/total_gacha_");
		strcat(name,tmp);
		makefolder(name);
		for(int i = 0; i<9; i++){
			strcpy(path, "./gacha_gacha/");
			strcpy(path,txtname[i]);
			movefile(path,name);
			deletefile(path);	
		}
		return (gacha/10);
	}
	```
	proses penamaan dilakukan sesuai dengan prosedur.
	pada fungsi diatas terdapat fungsi cabang untuk memindahkan file dan menghapus file, kedua fungsi tersebut akan di jelaskan di akhir soal.

### Soal 1.d ###
Pada game tersebut, untuk melakukan gacha item kita harus menggunakan alat tukar yang dinamakan primogems. Satu kali gacha item akan menghabiskan primogems sebanyak 160 primogems. Karena mas Refadi ingin agar hasil simulasi gacha nya terlihat banyak, maka pada program, primogems di awal di-define sebanyak 79000 primogems. Setiap kali gacha, ada 2 properties yang akan diambil dari database, yaitu name dan rarity. Lalu Outpukan hasil gacha nya ke dalam file (.txt) dengan format hasil gacha {jumlah-gacha}_[tipe-item]_{rarity}_{name}_{sisa-primogems}. Program akan selalu melakukan gacha hingga primogems habis.
Contoh : 157_characters_5_Albedo_53880

pada soal ini terdapat beberapa perintah :
1. proses gacha dilakukan dengan bekal uang 79000 dengan harga setiap gacha 160, hingga uang habis

	**Fungsi utama**
	```c
			int gachaxx = 0;
			int money = 79000;
			int foldercheck = 1;
			int temp;
			while (1){
				if (money <0){ 
				printf("uang kenakalan sudah habis!!");
				return 0;
				}
				printf("tekan banyak gacha unutuk maksiat, 0 jika sedang miskin\n");
				scanf("%d", &temp);
				if (temp == 0) 
					return 0;

				printf("selamat anda mendapatkan\n");
				for (int i = 0; i < temp ; i++){
					money = money - 160;
					}
				}
			}
	```
	pada fungsi utama dinerikan nilai ariabel uang sejumlah 79000, pada setiap gacha uang akan berkurang 160, pada fungsi utama juga diberikan pengaman jika uang kurang dari 0 akan menghentikan program
2. Proses penamaan menggunakan `{jumlah-gacha}_[tipe-item]_{rarity}_{name}_{sisa-primogems}`

	**Penamaan untuk Weapon**
	```c
		strcpy(hero, "./weapon/");
		strcat(hero, weapon[rndgnet]);
		sprintf(tempresult[gachaxx%10], "%d", gachaxx+1);
		strcat(tempresult[gachaxx%10], "_Waepon_");
		strcat(tempresult[gachaxx%10], rarity(hero));
		strcat(tempresult[gachaxx%10], "_");
		strcat(tempresult[gachaxx%10], getname(hero));
		strcat(tempresult[gachaxx%10], "_");
		sprintf(temp, "%d", money);
		strcat(tempresult[gachaxx%10], temp);
	```

	**Penamaan Untuk Character**
	```c
		strcpy(hero, "./characters/");
    	strcat(hero, characters[rndgnet]);
		sprintf(tempresult[gachaxx%10], "%d", gachaxx+1);
		strcat(tempresult[gachaxx%10], "_characters_");
		strcat(tempresult[gachaxx%10], raritychr(hero));
		strcat(tempresult[gachaxx%10], "_");
		strcat(tempresult[gachaxx%10], getnamechr(hero));
		strcat(tempresult[gachaxx%10], "_");
		sprintf(temp, "%d", money);
		strcat(tempresult[gachaxx%10], temp);
		printf(">>> %s\n", tempresult[gachaxx%10]);
	```
	pada fungsi di atas terdata beberapa bagian fungsi utntuk mendapatkan rarity dan nama dari hero maupun character, fungsi selanjutnya akan dijelaskan di akhir soal.

### Soal 1.e ###
Proses untuk melakukan gacha item akan dimulai bertepatan dengan anniversary pertama kali mas Refadi bermain bengshin impek, yaitu pada 30 Maret jam 04:44.  Kemudian agar hasil gacha nya tidak dilihat oleh teman kos nya, maka 3 jam setelah anniversary tersebut semua isi di folder gacha_gacha akan di zip dengan nama not_safe_for_wibu dengan dipassword "satuduatiga", lalu semua folder akan di delete sehingga hanya menyisakan file (.zip)

```c
void timecounter(){
	pid_t child_id;
	child_id = fork();	
	char arr[80];
    time_t cek = time(NULL);
    char timetogo[10];
    struct tm timenow = *localtime(&cek);
    strftime(arr, sizeof(arr)-1, "%d-%m_%H:%M", &timenow);
    //generate time eksekuci
    strcpy(timetogo, "30-04_04:44");
	 
	if (child_id < 0) {
		exit(EXIT_FAILURE);
	}
	if (child_id == 0) {
	} 	
	else {
	while(strcmp(arr,timetogo)!=0) {
		cek = time(NULL);
		timenow = *localtime(&cek);
		strftime(arr, sizeof(arr)-1, "%d-%m_%H:%M", &timenow);
		sleep(10);
	}
   	char *argv2[] = {"zip","-r","not_safe_for_wibu.zip","-P", "satuduatiga", "gacha_gacha", NULL};
	execv("/bin/zip", argv2);   
	}
};
```
pada proses ini dilakukan pengecekan berulang untuk mendapatkan waktu ketika jam eksekusi, proses komparasi menggunakan fungsi `strcmp`, jika ditemukan kondisi yang sesuai maka akan dilakukan proses zip ber password dengan menggunakan `execv`

### Fungsi Pendukung ###

**Fungsi movefile()**
```c
void movefile(char filename[15], char path[30]) {
    pid_t child_id;
    int status;
    child_id = fork();
    if (child_id < 0) {
      exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
     }
     if (child_id == 0) {
       char *argv[6] = {"cp", filename, path, NULL};
       execv("/bin/cp", argv);
      } else {
        while((wait(&status)) > 0);
        return;       
      }
}
```

**deletefile**
```c
void deletefile(char filename[30]) {
    pid_t child_id;
    int status;
    child_id = fork();
    if (child_id < 0) {
      exit(EXIT_FAILURE);
     }
     if (child_id == 0) {
	char *argv1[] = {"rm", "-r", filename, NULL};
	execv("/bin/rm", argv1);
      } else {
        while((wait(&status)) > 0);
        return;       
      }
}
```

**Mendapatkan Rarity untuk weapon**
```c
const char* rarity(char heroname[70]) {
	FILE *fp;
	char buffer[1024];
	struct json_object *parsed_json;
	struct json_object *rarity;
	size_t n_friends;

	size_t i;	
	char tmp[40];
	fp = fopen(heroname,"r");
	fread(buffer, 1024, 1, fp);
	fclose(fp);
	for (int i = 30 ; i <2000;i++){
		if ( buffer[i] == 'b' && buffer[i+1] == 'a' && buffer[i+2] == 's'&&buffer[i+3] == 'e') { 
			buffer[i-2] = '}';
			buffer[i-1] = '\n';
			break;
			}
	}

	parsed_json = json_tokener_parse(buffer);
	json_object_object_get_ex(parsed_json, "rarity", &rarity);
	return json_object_get_string(rarity);
}
```

**Mendapatkan Rarity untuk Character**
```c
const char* raritychr(char heroname[50]) {
	FILE *fp;
	char buffer[1024];
	struct json_object *parsed_json;
	struct json_object *rarity;
	size_t n_friends;

	size_t i;	
	char tmp[40];
	fp = fopen(heroname,"r");
	fread(buffer, 1024, 1, fp);
	fclose(fp);
	for (int i = 30 ; i <2000;i++){
		if ( buffer[i] == 'e' && buffer[i+1] == 'l' && buffer[i+2] == 'e'&&buffer[i+3] == 'm') { 
			buffer[i-2] = '}';
			buffer[i-1] = '\n';
			break;
			}
	}

	parsed_json = json_tokener_parse(buffer);
	json_object_object_get_ex(parsed_json, "rarity", &rarity);
	return json_object_get_string(rarity);
}
```

**Mendapatkan Name untuk Weapon**
```c
const char* getname(char heroname[70]) {
	FILE *fp;
	char buffer[1024];
	struct json_object *parsed_json;
	struct json_object *name;
	size_t n_friends;
	size_t i;	
	fp = fopen(heroname,"r");
	fread(buffer, 1024, 1, fp);
	fclose(fp);
	for (int i = 30 ; i <2000;i++){
		if ( buffer[i] == 'b' && buffer[i+1] == 'a' && buffer[i+2] == 's'&&buffer[i+3] == 'e') { 
			buffer[i-2] = '}';
			buffer[i-1] = '\n';
			break;
			}
	}

	parsed_json = json_tokener_parse(buffer);
	json_object_object_get_ex(parsed_json, "name", &name);
	return json_object_get_string(name);	
}
```

**Mendapatkan Name Untuk Character**
```c
const char* getnamechr(char heroname[30]) {
	FILE *fp;
	char buffer[1024];
	struct json_object *parsed_json;
	struct json_object *name;
	size_t n_friends;

	size_t i;	

	fp = fopen(heroname,"r");
	fread(buffer, 1024, 1, fp);
	fclose(fp);
	for (int i = 30 ; i <2000;i++){
		if ( buffer[i] == 'e' && buffer[i+1] == 'l' && buffer[i+2] == 'e'&&buffer[i+3] == 'm') { 
			buffer[i-2] = '}';
			buffer[i-1] = '\n';
			break;
			}
	}

	parsed_json = json_tokener_parse(buffer);
	json_object_object_get_ex(parsed_json, "name", &name);
	return json_object_get_string(name);	
}
```

## Nomer 2 ##
Japrun bekerja di sebuah perusahaan dibidang review industri perfilman, karena kondisi saat ini sedang pandemi Covid-19, dia mendapatkan sebuah proyek untuk mencari drama korea yang tayang dan sedang ramai di Layanan Streaming Film untuk diberi review. Japrun sudah mendapatkan beberapa foto-foto poster serial dalam bentuk zip untuk diberikan review, tetapi didalam zip tersebut banyak sekali poster drama korea dan dia harus memisahkan poster-poster drama korea tersebut tergantung dengan kategorinya. Japrun merasa kesulitan untuk melakukan pekerjaannya secara manual, kamu sebagai programmer diminta Japrun untuk menyelesaikan pekerjaannya.

### Soal 2.a ###
Hal pertama yang perlu dilakukan oleh program adalah mengextract zip yang diberikan ke dalam folder “/home/[user]/shift2/drakor”. Karena atasan Japrun teledor, dalam zip tersebut bisa berisi folder-folder yang tidak penting, maka program harus bisa membedakan file dan folder sehingga dapat memproses file yang seharusnya dikerjakan dan menghapus folder-folder yang tidak dibutuhkan.
<br><br>
Dalam penyelesaian ini, akan digunakan fungsi-fungsi terpisah, masing-masing dengan fork,execv,wait untuk dapat menjalankan fungsional yang dibutuhkan.
Pertama, dibuat folder directory yang akan menjadi tempat .zip file akan diextract
```c
void addFolder1(){
	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id < 0) {
    	exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }
    if (child_id == 0) {
			char *argv[] = {"mkdir", "-p", "user/shift2/drakor", NULL};
			execv("/bin/mkdir", argv);
		} 
	else {
		while ((wait(&status)) > 0);
		unzipz();
    }
}
```

Lalu dilanjutkan dengan menunzip drakor.zip ke dalam folder directory tadi. 
```c
void unzipz(){
	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id < 0) {
    	exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }
    if (child_id == 0) {
			char *argv[] = {"unzip", "drakor.zip", "-d", "user/shift2/drakor/", NULL};
			execv("/bin/unzip", argv);
		} 
	else {
		while ((wait(&status)) > 0);
		filters();
	}
}
```

Lalu, kita coba looping isi dari folder hasil unzip sebelumnya dan mengecek apabila namanya tidak berakhiran dengan ".png", maka itu termasuk isi yang tidak penting dan kita delete menggunakan execv rm
```c
void filters(){
	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id < 0) {
    	exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }
    if (child_id == 0) {
			DIR *d;
			struct dirent *dir;
			d = opendir("user/shift2/drakor/");
			if(d){
				while ((dir = readdir(d)) != NULL){
					char strz[100] = "";
					strcpy(strz, dir->d_name);
					int lngth = strlen(strz);
					if(strz[lngth-4] != '.' || strz[lngth-3] != 'p' || strz[lngth-2] != 'n' || strz[lngth-1] != 'g'){
						printf("Hasilnya : %s\n", strz);
						char delz[200] = "user/shift2/drakor/";
						strcat(delz, strz);
						if(fork() == 0){
							char *argv[] = {"rm", "-r", delz, NULL};
							execv("/bin/rm", argv);
						}
					}
				}
				closedir(d);
			}
		} 
	else {
		while ((wait(&status)) > 0);
		addFolder2();
    }
}
```


### Soal 2.b ###
Poster drama korea perlu dikategorikan sesuai jenisnya, maka program harus membuat folder untuk setiap jenis drama korea yang ada dalam zip. Karena kamu tidak mungkin memeriksa satu-persatu manual, maka program harus membuatkan folder-folder yang dibutuhkan sesuai dengan isi zip.
Contoh: Jenis drama korea romance akan disimpan dalam “/drakor/romance”, jenis drama korea action akan disimpan dalam “/drakor/action” , dan seterusnya.
<br><br>
Pertama, kita akan looping lagi directory tempat files poster berada, lalu kita iterasikan nama dari tiap file poster tersebut dan mengambil substring untuk jenis kategori tiap poster. Lalu kita buat directory baru berdasarkan jenis kategori yang telah didapatkan. 
```c
void addFolder2(){
	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id < 0) {
    	exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }
    if (child_id == 0) {
			DIR *d;
			struct dirent *dir;
			d = opendir("user/shift2/drakor/");
			if(d){
				while ((dir = readdir(d)) != NULL){
					char strz[100] = "";
					strcpy(strz, dir->d_name);
					int lngth = strlen(strz);
					int sect = 0;
					char strJenis[10][20] = {"", "", "", "", "", "", "", "", "", ""};
					int a = 0;
					int b = 0;
					//printf("%s\n", strz);
					for(int j = 0; j < lngth; j++){
						if(strz[j] == ';'){
							sect++;
						}
						else if(strz[j] == '_' || strz[j] == '.'){
							a++;
							b = 0;
							sect++;
						}
						else if(sect == 2 || sect == 5){
							strJenis[a][b] = strz[j];
							b++;
						}
					}
					for(int x = 0; x < a; x++){
						char addz[200] = "user/shift2/drakor/";
						strcat(addz, strJenis[x]);
						if(fork() == 0){
							char *argv[] = {"mkdir", addz, NULL};
							execv("/bin/mkdir", argv);
						}
						wait(NULL);
						strcat(addz, "/data.txt");
						FILE *f;
						f = fopen(addz, "w+");
						fprintf(f, "kategori : %s\n\n\n", strJenis[x]);
						fclose(f);
					}
				}
				closedir(d);
			}
		} 
	else {
		while ((wait(&status)) > 0);
		addDatas();
	}
}
```

### Soal 2.c ###
Setelah folder kategori berhasil dibuat, program akan memindahkan poster ke folder dengan kategori yang sesuai dan di rename dengan nama.
Contoh: “/drakor/romance/start-up.png”.
<br><br>
Sekali lagi, kita looping folder directory tempat files poster berada, namun kali ini kita cek khusus untuk yang namanya berekstensi ".png", hal ini diperlukan karena di folder directory tersebut sekarang telah terdapat folder tiap kategori poster. Kemudian, kita iterasikan nama tiap file poster tersebut dan kita ambil substring nama poster dan jenis kategori poster. Dengan 2 substring tersebut diketahui, kita dapat melakukan move file poster ke folder kategori yang sesuai dengan nama yang diminta soal
```c
void addDatas(){
	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id < 0) {
    	exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }
    if (child_id == 0) {
			DIR *d;
			struct dirent *dir;
			d = opendir("user/shift2/drakor/");
			if(d){
				while ((dir = readdir(d)) != NULL){
					char strz[100] = "";
					strcpy(strz, dir->d_name);
					int lngth = strlen(strz);
					if(strz[lngth-4] == '.' && strz[lngth-3] == 'p' && strz[lngth-2] == 'n' && strz[lngth-1] == 'g'){
						int sect = 0;
						char strNama[100] = "";
						char strJenis[20] = "";
						char strTahun[20] = "";
						int a = 0;
						int b = 0;
						
						for(int j = 0; j < lngth; j++){
							
							if(strz[j] == ';'){
								sect++;
							}
							else if(strz[j] == '_' || strz[j] == '.'){
								//char renamez[200] = "user/shift2/drakor/";
								char movez[200] = "user/shift2/drakor/";
								char movezFile[200] = "user/shift2/drakor/";
								char writeData[200] = "user/shift2/drakor/";

								strcat(movezFile, strz);
								//printf("%s\n", strJenis);
									
								strcat(movez, strJenis);
								strcat(movez, "/");
								strcat(movez, strNama);
									
								//printf("%s\n%s\n%s\n\n", movezFile, movez, renamez);
								strcat(writeData, strJenis);
								strcat(writeData, "/data.txt");
								
								FILE *f;
								f = fopen(writeData, "a");
								fprintf(f, "nama : %s\nrilis : tahun %s\n\n", strNama, strTahun);
								fclose(f);
								printf("\n--------------\n%s\n--------------\n", strNama);
								if(strz[j] == '_'){
									if(fork() == 0){
										char *argv[] = {"cp", movezFile, movez, NULL};
										execv("/bin/cp", argv);
									}
								}
								else{
									if(fork() == 0){
										char *argv[] = {"mv", movezFile, movez, NULL};
										execv("/bin/mv", argv);
									}
								}
								wait(NULL);
									
								a = 0;
								b = 0;
								char strNama[100] = "";
								strcpy(strJenis, "");
								strcpy(strTahun, "");
									
								sect++;
							}
							else if(sect == 0 || sect == 3){
								strNama[a] = strz[j];
								a++;
							}
							else if(sect == 2 || sect == 5){
								strncat(strJenis, &strz[j], 1);
							}
							else if(sect == 1 || sect == 4){
								strncat(strTahun, &strz[j], 1);
							}
						}
					}
				}
				closedir(d);
			}
		} 
	else {
		while ((wait(&status)) > 0);
		printf("Oke");
		char *argv[] = {"rm", "user/shift2/drakor/data.txt", NULL};
		execv("/bin/rm", argv);
  }
}
```

### Soal 2.d ###
Karena dalam satu foto bisa terdapat lebih dari satu poster maka foto harus di pindah ke masing-masing kategori yang sesuai. Contoh: foto dengan nama “start-up;2020;romance_the-k2;2016;action.png” dipindah ke folder “/drakor/romance/start-up.png” dan “/drakor/action/the-k2.png”. (note 19/03: jika dalam satu foto ada lebih dari satu poster maka foto tersebut dicopy jadi akhirnya akan jadi 2 foto)
<br><br>
Untuk melakukan ini, dapat dengan mudah kita lakukan dengan mengecek terlebih dahulu apakah file poster pada iterasi saat ini berisikan 2 poster atau 1 saja. Apabila berisikan 2 maka, untuk yang pertama, kita copykan, kemudian yang kedua baru kita move. Bila hanya berisikan 1 maka, dapat langsung kita move ke folder kategori yang sesuai. (Contoh kode sudah ada pada 2.c)


### Soal 2.e ###
Di setiap folder kategori drama korea buatlah sebuah file "data.txt" yang berisi nama dan tahun rilis semua drama korea dalam folder tersebut, jangan lupa untuk sorting list serial di file ini berdasarkan tahun rilis (Ascending). Format harus sesuai contoh 
<br><br>
Pertama, untuk membuat data.txt pada tiap folder kategori terlebih dahulu, dapat kita gabungkan fungsinya dalam fungsi 2.b sehingga pada tiap pembuatan folder kategori, akan terbentuk juga file data.txt di dalamnya (Contoh kode sudah ada pada 2.b)

Lalu untuk mengisikan nama dan tahun setiap poster, dapat juga kita gabungkan fungsinya ke dalam fungsi 2.c sehingga pada tiap pemindahan poster ke folder kategorinya, nama dan tahun rilis dari poster tersebut akan tertulis ke dalam data.txt pada folder kategorinya sesuai format yang diminta (Contoh kode sudah ada pada 2c)



## Nomer 3 ##
Conan adalah seorang detektif terkenal. Suatu hari, Conan menerima beberapa laporan tentang hewan di kebun binatang yang tiba-tiba hilang. Karena jenis-jenis hewan yang hilang banyak, maka perlu melakukan klasifikasi hewan apa saja yang hilang

### Soal 3.a ###
Untuk mempercepat klasifikasi, Conan diminta membuat program untuk membuat 2 directory di “/home/[USER]/modul2/” dengan nama “darat” lalu 3 detik kemudian membuat directory ke 2 dengan nama “air”. 

Pertama dalam program main membuat program untuk creat folder modul2 kemudian setelah create modul2 akan mengeksekusi fungsi fungsi yang sudah di buat sesuai dengan perintah soal.

**create folder modul2**
```c
nt main(){
	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id < 0) {
    	exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }
    if (child_id == 0) {
	//membuat folder modul2
		char *argv[6] = {"mkdir","-p","/home/khalidjuna/modul2",NULL};
        execv("/bin/mkdir", argv);
	} 
```
setelah selesai membuat folder modul2, program akan menjalan kan perintah selanjutnya,yaitu create folder 'darat'.
Di dalam fungsi main program akan menjalankan fungsi 'createdarat' yang sudah di buat :

```c
else {
		while ((wait(&status)) > 0);
		createdarat();
    }
}
```
***createdarat***
```c
void createdarat(){
	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id < 0) {
    	exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }
    if (child_id == 0) {
	//membuat folder darat
		char *argv[6] = {"mkdir","-p","/home/khalidjuna/modul2/darat",NULL};
        execv("/bin/mkdir", argv);
	} 
	else {
		while ((wait(&status)) > 0);
		sleep(3);
		createair();

    }
}
```
diatas adalah program untuk membuat folder darat, karena di dalam soal setelah membuat folder darat akan membuat folder air degan jarak waktu 3 detik, maka di dalam else di beri perintah sleep(3); kemudian setelah 3 detik akan lanjut menjalankan program di bawahnya 'createair' yaitu  create folder air.

***createair***
```c
void createair(){
	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id < 0) {
    	exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }
    if (child_id == 0) {
	//membuat folder air
		char *argv[6] = {"mkdir","-p","/home/khalidjuna/modul2/air",NULL};
        execv("/bin/mkdir", argv);
	} 
```

### Soal 3.b ###
Kemudian program diminta dapat melakukan extract “animal.zip” di “/home/[USER]/modul2/”.

***extract file animal.zip di dalam modul2**
```c
void unzipdownloader(){
	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id < 0) {
		exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }
	if(child_id==0){
		char *argv[10]= {"unzip","-j","animal.zip","-d","/home/khalidjuna/modul2/",NULL};
		execv("/bin/unzip", argv); 
	}
```
diatas adalah program  untuk mengextract file "animal.zip", disini mengunakan fungsi "unzip"

### Soal 3.c ###
Tidak hanya itu, hasil extract dipisah menjadi hewan darat dan hewan air sesuai dengan nama filenya. Untuk hewan darat dimasukkan ke folder “/home/[USER]/modul2/darat” dan untuk hewan air dimasukkan ke folder “/home/[USER]/modul2/air”. Rentang pembuatan antara folder darat dengan folder air adalah 3 detik dimana folder darat dibuat terlebih dahulu. Untuk hewan yang tidak ada keterangan air atau darat harus dihapus.

Setelah file "animal.zip" di extract, selanjutnya kita disuruh untuk memisahkan hewan darat dan air ke folder masing-masing yang sudah di buat dalam soal pertama.
Sebelum itu kami melakukan pegecekan terlebih dahulu semua file yang ada di modul2 dengan membuat fungsi "filtering1".

```c
void filtering1(){
	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id < 0) {
		exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }
	if(child_id==0){
		DIR *d;
		struct dirent *dir;
		d = opendir("/home/danialfarros/modul2/");
		if (d){
		int count =0;
		char temp[50];
		while ((dir = readdir(d)) != NULL){
			if (strlen(dir->d_name) > 5){
				strcpy(temp,"/home/danialfarros/modul2/");
				strcat(temp, dir->d_name);
				check(temp);
				//printf("%s/n", dir->d_name);
				count++;
				}
			}
		}
		closedir(d);
```

Setelah dilakukan pengecekan di dalam folder "modul2" program akan menjalankan perintah selanjutnya, yaitu memasukkan file darat ke folder darat dan air ke dalam folder air. Disini kami membuat fungsi 'int check' sehingga akan memanggil fungsi check untuk memisahkan file.

***int check***
```c
int check(char tmp[50]){
    char *ret;
	if (strstr(tmp, "darat")){
		movefile (tmp, "/home/khalidjuna/modul2/darat");
		deletefile(tmp);
	}

	else if (strstr(tmp, "air")){
		movefile (tmp, "/home/khalidjuna/modul2/air");
    	deletefile(tmp);
    }
    else 
    	deletefile(tmp);
}
``` 
Program di atas untuk memindahkan file sesuai dengan folder masing-masing. Karena di dalam soal di suruh menghapus file yang tidak ada keterangan 'darat' atau 'air' maka file yang tersisa akan di hapus dengan perintah:
```c
 else 
    	deletefile(tmp);
```

### Soal 3.d ###
Setelah berhasil memisahkan hewan berdasarkan hewan darat atau hewan air. Dikarenakan jumlah burung yang ada di kebun binatang terlalu banyak, maka pihak kebun binatang harus merelakannya sehingga conan harus menghapus semua burung yang ada di directory “/home/[USER]/modul2/darat”. Hewan burung ditandai dengan adanya “bird” pada nama file.

Setelah menjalankan fungsi 'filtering1' program lanjut menjalankan 'filtering2' dengan perintah: 
```c
else{
	while ((wait(&status)) > 0);
	filtering2();
	}
``` 
filtering2 ini untuk mengecek folder 'darat' yang di dalamnya terdapat binatang burung. 

***filering2***
```c
pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id < 0) {
		exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }
	if(child_id==0){
		DIR *d;
		struct dirent *dir;
		d = opendir("/home/khalidjuna/modul2/darat");
		if (d){
		int count =0;
		char temp[50];
		while ((dir = readdir(d)) != NULL){
			if (strlen(dir->d_name) > 5){
				strcpy(temp,"/home/khalidjuna/modul2/darat/");
				strcat(temp, dir->d_name);
				check2(temp);
				//printf("%s/n", dir->d_name);
				count++;
				}
			}
		}
		closedir(d);

	}
```
setelah melakukan pengecekan di dalam folder 'darat' program lanjut menjalankan fungsi 'check2'. Fungsi ini kami buat untuk mencari file yang di tandai dengan 'bird' kemudian di hapus dari folder darat.

***check2***
```c
int check2(char tmp[50]){
    char *ret;
	if (strstr(tmp, "bird"))
		deletefile(tmp);
}
```

### Soal 3.e ###
Terakhir, Conan harus membuat file list.txt di folder “/home/[USER]/modul2/air” dan membuat list nama semua hewan yang ada di directory “/home/[USER]/modul2/air” ke “list.txt” dengan format UID_[UID file permission]_Nama File.[jpg/png] dimana UID adalah user dari file tersebut file permission adalah permission dari file tersebut.
Contoh : conan_rwx_hewan.png

program lanjut menjalankan perintah di 'filtering2' yaitu menjalankan 'filtering3':
```c
else{
	while ((wait(&status)) > 0);
	filtering3();
	}
```
'filtering3' ini kami buat untuk mengecek file yang berada pada folder air karena conan harus membuat file list.txt pada folder air untuk membuat list nama semua hewan air.

***filtering3***
```c
void filtering3(){
	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id < 0) {
		exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }
	if(child_id==0){
		DIR *d;
		struct dirent *dir;
		d = opendir("/home/khalidjuna/modul2/air");
		if (d){
		makefile("/home/khalidjuna/modul2/air/list.txt");
		int count =0;
		//char temp[50];
		FILE * fPtr;
		fPtr = fopen("/home/khalidjuna/modul2/air/list.txt", "w");
		if(fPtr == NULL){
        	printf("Unable to create file.\n");
    	}
		
		while ((dir = readdir(d)) != NULL){
			if (strlen(dir->d_name) > 5){
				fputs(check3(dir->d_name),fPtr);
    			fputs("\n",fPtr);
				count++;
				}
			}
		fclose(fPtr);	
		}
		closedir(d);

	}
```
setelah membuka folder air, lanjut membuat file list.txt pada folder air dengan perintah:
```c
if (d){
		makefile("/home/khalidjuna/modul2/air/list.txt");
```
kemudian program lanjut menjalankan:
```c
FILE * fPtr;
		fPtr = fopen("/home/khalidjuna/modul2/air/list.txt", "w");
```
fungsinya untuk membuka file list.txt yang sudah di buat dengan perintah 'w' untuk menulis semua nama file air ke dalamnya.

terakhir conan di suruh membuat penamaan file dengan format UID_[UID file permission]_Nama File.[jpg/png] dimana UID adalah user dari file tersebut file permission adalah permission dari file tersebut.
Makan program akan lanjut menjalankan perintah:
```c
while ((dir = readdir(d)) != NULL){
			if (strlen(dir->d_name) > 5){
				fputs(check3(dir->d_name),fPtr);
    			fputs("\n",fPtr);
				count++;
				}
			}
```
kami membuat fungsi check3:
```c
const char* check3(char ending[100]) {
    char path[100];
    strcpy(path,"/home/khalidjuna/modul2/air/");
    strcat(path,ending);
    char name[30];
    strcpy(name,ending);
    struct stat info;
    int r;
	struct stat fs;
    r = stat(path, &info);
    r = stat(path,&fs);
    if( r==-1 )
    {
        fprintf(stderr,"File error\n");
        exit(1);
    }

    struct passwd *pw = getpwuid(info.st_uid);
    struct group  *gr = getgrgid(info.st_gid);
    strcpy(ending,pw->pw_name);
    strcat(ending,"_");
    //printf("Owner permissions: ");
    if( fs.st_mode & S_IRUSR )
     	strcat(ending,"R");
        //printf("R");
    if( fs.st_mode & S_IWUSR )
    strcat(ending,"W");
        //printf("W");
    if( fs.st_mode & S_IXUSR )
        strcat(ending,"E");
        //printf("E");
    strcat(ending,"_");
    strcat(ending,name);
    return ending;
}
```
fungsi const char* check3 membuat char ending lalu mengcopy file di dalam char path /air/ selanjut nya di gabung dengan ending. Lalu char name di gabung dengan ending.
diatas terdapat perintah:
```c
struct passwd *pw = getpwuid(info.st_uid);
    struct group  *gr = getgrgid(info.st_gid);
    strcpy(ending,pw->pw_name);
    strcat(ending,"_");
```
fungsinya untuk mendapatkan pwuid dan grgid, kemudian pwpw->pw_name di copy di dalam variabel ending kemudian di beri '_' di setiap spasi untuk penamaan filenya.
terakhir program akan (return ending;) sehingga penamaan file akan sesuai dengan yang di inginkan.
