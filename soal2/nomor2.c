#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <dirent.h>
#include <string.h>

void addDatas(){
	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id < 0) {
    	exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }
    if (child_id == 0) {
			DIR *d;
			struct dirent *dir;
			d = opendir("user/shift2/drakor/");
			if(d){
				while ((dir = readdir(d)) != NULL){
					char strz[100] = "";
					strcpy(strz, dir->d_name);
					int lngth = strlen(strz);
					if(strz[lngth-4] == '.' && strz[lngth-3] == 'p' && strz[lngth-2] == 'n' && strz[lngth-1] == 'g'){
						int sect = 0;
						char strNama[100] = "";
						char strJenis[20] = "";
						char strTahun[20] = "";
						int a = 0;
						int b = 0;
						
						for(int j = 0; j < lngth; j++){
							
							if(strz[j] == ';'){
								sect++;
							}
							else if(strz[j] == '_' || strz[j] == '.'){
								//char renamez[200] = "user/shift2/drakor/";
								char movez[200] = "user/shift2/drakor/";
								char movezFile[200] = "user/shift2/drakor/";
								char writeData[200] = "user/shift2/drakor/";

								strcat(movezFile, strz);
								//printf("%s\n", strJenis);
									
								strcat(movez, strJenis);
								strcat(movez, "/");
								strcat(movez, strNama);
									
								//printf("%s\n%s\n%s\n\n", movezFile, movez, renamez);
								strcat(writeData, strJenis);
								strcat(writeData, "/data.txt");
								
								FILE *f;
								f = fopen(writeData, "a");
								fprintf(f, "nama : %s\nrilis : tahun %s\n\n", strNama, strTahun);
								fclose(f);
								printf("\n--------------\n%s\n--------------\n", strNama);
								if(strz[j] == '_'){
									if(fork() == 0){
										char *argv[] = {"cp", movezFile, movez, NULL};
										execv("/bin/cp", argv);
									}
								}
								else{
									if(fork() == 0){
										char *argv[] = {"mv", movezFile, movez, NULL};
										execv("/bin/mv", argv);
									}
								}
								wait(NULL);
								//printf("\n%s\n", strz);
									
									
									//wait(NULL);
									//if(fork() == 0){
									//	char *argv[] = {"mv", renamez, NULL};
									//	execv("/bin/mv", argv);
									//}
									//printf("debug2\n");
									
								a = 0;
								b = 0;
								char strNama[100] = "";
								strcpy(strJenis, "");
								strcpy(strTahun, "");
								//char strJenis[20] = "";
									//char strJenis[20] = "";
									
								sect++;
							}
							else if(sect == 0 || sect == 3){
								strNama[a] = strz[j];
								a++;
							}
							else if(sect == 2 || sect == 5){
								strncat(strJenis, &strz[j], 1);
							}
							else if(sect == 1 || sect == 4){
								strncat(strTahun, &strz[j], 1);
							}
						}
					}
				}
				closedir(d);
			}
		} 
	else {
		while ((wait(&status)) > 0);
		printf("Oke");
		char *argv[] = {"rm", "user/shift2/drakor/data.txt", NULL};
		execv("/bin/rm", argv);
  }
}

void addFolder2(){
	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id < 0) {
    	exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }
    if (child_id == 0) {
			DIR *d;
			struct dirent *dir;
			d = opendir("user/shift2/drakor/");
			if(d){
				while ((dir = readdir(d)) != NULL){
					char strz[100] = "";
					strcpy(strz, dir->d_name);
					int lngth = strlen(strz);
					int sect = 0;
					char strJenis[10][20] = {"", "", "", "", "", "", "", "", "", ""};
					int a = 0;
					int b = 0;
					//printf("%s\n", strz);
					for(int j = 0; j < lngth; j++){
						if(strz[j] == ';'){
							sect++;
						}
						else if(strz[j] == '_' || strz[j] == '.'){
							a++;
							b = 0;
							sect++;
						}
						else if(sect == 2 || sect == 5){
							strJenis[a][b] = strz[j];
							b++;
						}
					}
					for(int x = 0; x < a; x++){
						char addz[200] = "user/shift2/drakor/";
						strcat(addz, strJenis[x]);
						if(fork() == 0){
							char *argv[] = {"mkdir", addz, NULL};
							execv("/bin/mkdir", argv);
						}
						wait(NULL);
						strcat(addz, "/data.txt");
						FILE *f;
						f = fopen(addz, "w+");
						fprintf(f, "kategori : %s\n\n\n", strJenis[x]);
						fclose(f);
					}
				}
				closedir(d);
			}
		} 
	else {
		while ((wait(&status)) > 0);
		addDatas();
	}
}


void filters(){
	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id < 0) {
    	exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }
    if (child_id == 0) {
			DIR *d;
			struct dirent *dir;
			d = opendir("user/shift2/drakor/");
			if(d){
				while ((dir = readdir(d)) != NULL){
					char strz[100] = "";
					strcpy(strz, dir->d_name);
					int lngth = strlen(strz);
					if(strz[lngth-4] != '.' || strz[lngth-3] != 'p' || strz[lngth-2] != 'n' || strz[lngth-1] != 'g'){
						printf("Hasilnya : %s\n", strz);
						char delz[200] = "user/shift2/drakor/";
						strcat(delz, strz);
						if(fork() == 0){
							char *argv[] = {"rm", "-r", delz, NULL};
							execv("/bin/rm", argv);
						}
					}
				}
				closedir(d);
			}
		} 
	else {
		while ((wait(&status)) > 0);
		addFolder2();
    }
}

void unzipz(){
	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id < 0) {
    	exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }
    if (child_id == 0) {
			char *argv[] = {"unzip", "drakor.zip", "-d", "user/shift2/drakor/", NULL};
			execv("/bin/unzip", argv);
		} 
	else {
		while ((wait(&status)) > 0);
		filters();
	}
}

void addFolder1(){
	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id < 0) {
    	exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }
    if (child_id == 0) {
			char *argv[] = {"mkdir", "-p", "user/shift2/drakor", NULL};
			execv("/bin/mkdir", argv);
		} 
	else {
		while ((wait(&status)) > 0);
		unzipz();
    }
}

int main()
{
	pid_t child_id;
	int status;
	
	child_id = fork();
	
	if(child_id < 0){
		exit(EXIT_FAILURE);
	}
	if(child_id > 0){
		addFolder1();	
		
		}
	else{
		printf("THis is child");
		//char *argv[] = {"mv", "drakor.zip", "./user/shift2/drakors/", NULL};
		//execv("/bin/mv", argv);
	}
	
	return 0;
}



