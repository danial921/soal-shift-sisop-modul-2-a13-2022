#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <wait.h>
#include <time.h>
#include<dirent.h>
#include <pwd.h>
#include <grp.h>
#include <sys/stat.h>


char foldername[25][40];

void movefile(char filename[50], char path[50]) {
    pid_t child_id;
    int status;

    child_id = fork();
 	
 	//strcat(path,filename);

    if (child_id < 0) {
      exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
     }

     if (child_id == 0) {
       // this is child
       
       //char *argv[6] = {"cp", filename, path, NULL};
       char *argv[6] = {"cp", filename, path, NULL};
       
       	//printf("file moving\n");
		//printf(">>file moving\n");

       execv("/bin/cp", argv);
      } else {
        while((wait(&status)) > 0);
        return;       
      }
}

void deletefile(char filename[50]) {
    pid_t child_id;
    int status;
    //strcat(filename,".txt");

    child_id = fork();

    if (child_id < 0) {
      exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
     }

     if (child_id == 0) {
	char *argv1[] = {"rm", "-r", filename, NULL};
	execv("/bin/rm", argv1);
      } else {
        while((wait(&status)) > 0);
        return;       
      }
}

void makefile(char index[50]){
	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id < 0) {
    	exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }
    if (child_id == 0) {
       // this is child
		//printf(">>create dir %s\n",index);
		char *argv[6] = {"touch",index,NULL};
		execv("/bin/touch", argv); 
	} 
	else {
    	while((wait(&status)) > 0);
    	return;
    }
}

int check2(char tmp[50]){
    char *ret;
	if (strstr(tmp, "bird"))
		deletefile(tmp);
}

int check(char tmp[50]){
    char *ret;
	if (strstr(tmp, "darat")){
		movefile (tmp, "/home/danialfarros/modul2/darat");
		deletefile(tmp);
	}

	else if (strstr(tmp, "air")){
		movefile (tmp, "/home/danialfarros/modul2/air");
    	deletefile(tmp);
    }
    else 
    	deletefile(tmp);
}

const char* check3(char ending[100]) {
    char path[100];
    strcpy(path,"/home/danialfarros/modul2/air/");
    strcat(path,ending);
    char name[30];
    strcpy(name,ending);
    struct stat info;
    int r;
	struct stat fs;
    r = stat(path, &info);
    r = stat(path,&fs);
    if( r==-1 )
    {
        fprintf(stderr,"File error\n");
        exit(1);
    }

    struct passwd *pw = getpwuid(info.st_uid);
    struct group  *gr = getgrgid(info.st_gid);
    strcpy(ending,pw->pw_name);
    strcat(ending,"_");
    //printf("Owner permissions: ");
    if( fs.st_mode & S_IRUSR )
     	strcat(ending,"R");
        //printf("R");
    if( fs.st_mode & S_IWUSR )
    strcat(ending,"W");
        //printf("W");
    if( fs.st_mode & S_IXUSR )
        strcat(ending,"E");
        //printf("E");
    strcat(ending,"_");
    strcat(ending,name);
    return ending;
}

void filtering3(){
	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id < 0) {
		exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }
	if(child_id==0){
		DIR *d;
		struct dirent *dir;
		d = opendir("/home/danialfarros/modul2/air");
		if (d){
		makefile("/home/danialfarros/modul2/air/list.txt");
		int count =0;
		//char temp[50];
		FILE * fPtr;
		fPtr = fopen("/home/danialfarros/modul2/air/list.txt", "w");
		if(fPtr == NULL){
        	printf("Unable to create file.\n");
    	}
		
		while ((dir = readdir(d)) != NULL){
			if (strlen(dir->d_name) > 5){
				//strcpy(temp,"/home/danialfarros/modul2/air/list.tx");
				//strcat(temp, dir->d_name);
				fputs(check3(dir->d_name),fPtr);
    			fputs("\n",fPtr);
				count++;
				}
			}
		fclose(fPtr);	
		}
		closedir(d);

	}
	else{
	while ((wait(&status)) > 0);
	printf("udah kelar nyok!! capet bat asyem!!\n");
	}
}

void filtering2(){
	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id < 0) {
		exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }
	if(child_id==0){
		DIR *d;
		struct dirent *dir;
		d = opendir("/home/danialfarros/modul2/darat");
		if (d){
		int count =0;
		char temp[50];
		while ((dir = readdir(d)) != NULL){
			if (strlen(dir->d_name) > 5){
				strcpy(temp,"/home/danialfarros/modul2/darat/");
				strcat(temp, dir->d_name);
				check2(temp);
				//printf("%s/n", dir->d_name);
				count++;
				}
			}
		}
		closedir(d);

	}
	else{
	while ((wait(&status)) > 0);
	filtering3();
	}
}

void filtering1(){
	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id < 0) {
		exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }
	if(child_id==0){
		DIR *d;
		struct dirent *dir;
		d = opendir("/home/danialfarros/modul2/");
		if (d){
		int count =0;
		char temp[50];
		while ((dir = readdir(d)) != NULL){
			if (strlen(dir->d_name) > 5){
				strcpy(temp,"/home/danialfarros/modul2/");
				strcat(temp, dir->d_name);
				check(temp);
				//printf("%s/n", dir->d_name);
				count++;
				}
			}
		}
		closedir(d);

	}
	else{
	while ((wait(&status)) > 0);
	filtering2();
	}
}

void unzipdownloader(){
	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id < 0) {
		exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }
	if(child_id==0){
		char *argv[10]= {"unzip","-j","animal.zip","-d","/home/danialfarros/modul2/",NULL};
		execv("/bin/unzip", argv); 
	}
	else{
	while ((wait(&status)) > 0);
	filtering1();
	}
}

void createair(){
	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id < 0) {
    	exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }
    if (child_id == 0) {
	//membuat folder modul2
		char *argv[6] = {"mkdir","-p","/home/danialfarros/modul2/air",NULL};
        execv("/bin/mkdir", argv);
	} 
	else {
		while ((wait(&status)) > 0);
		unzipdownloader();
    }
}
void createdarat(){
	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id < 0) {
    	exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }
    if (child_id == 0) {
	//membuat folder modul2
		char *argv[6] = {"mkdir","-p","/home/danialfarros/modul2/darat",NULL};
        execv("/bin/mkdir", argv);
	} 
	else {
		while ((wait(&status)) > 0);
		sleep(3);
		createair();

    }
}



int main(){
	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id < 0) {
    	exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }
    if (child_id == 0) {
	//membuat folder modul2
		char *argv[6] = {"mkdir","-p","/home/danialfarros/modul2",NULL};
        execv("/bin/mkdir", argv);
	} 
	else {
		while ((wait(&status)) > 0);
		createdarat();
    }
}